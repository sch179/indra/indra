from __future__ import annotations
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from glfw import *

init()
zoom = 1
def Rotate(x: complex) -> complex:
    return x * e ** (1j * 0.1)
def VisTrail(trail: list(complex)) -> None:
    glPointSize(5)
    glBegin(GL_POINTS)
    for i in trail:
        c = i
        c = c / zoom
        glVertex2d(c.real, c.imag)
    glEnd()
def CutTrail(trail: list(complex)) -> list(complex):
    if len(trail) > 30:
        return trail[-30:]
    else:
        return trail
    return trail
def f(x: complex) -> complex:
    return x * x * x * x.real
def AlteredTrail(f, trail: list(complex)):
    glPointSize(5)
    glBegin(GL_POINTS)
    for i in trail:
        c = f(i)
        c = c / zoom
        glVertex2d(c.real, c.imag)
    glEnd()
def MoveTrail(trail: list(complex)) -> list(complex):
    trail.append(Rotate(trail[-1]))
    trail = CutTrail(trail)
    VisTrail(trail)
    return trail
window = create_window(600, 600, "???", None, None)
make_context_current(window)
trail: list(complex) = list([0.5 + 0.5 * 1j])
while window_should_close(window) == FALSE:
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(0, 0, 1, 0.5)
    trail = MoveTrail(trail)
    AlteredTrail(f, trail)
    swap_buffers(window)
    poll_events()