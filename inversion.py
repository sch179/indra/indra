from __future__ import annotations
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
import cmath
m = 10
def Inv(c, r = 2 ** 0.5 / m, v = 0) -> complex:
    if c - v == 0:
        return 0
    return (r ** 2) / c.conjugate()
def VisFunc(f, cf):
    glBegin(GL_LINE_STRIP)
    for i in range(-100000, 100000):
        c = i * 0.001 + f(i * 0.001) * 1j
        ca = cf(c)
        glVertex2d(ca.real, ca.imag)
    glEnd()
def VisCenter(v: complex):
    glBegin(GL_POINTS)
    glColor3d(1, 0, 0)
    glVertex2d(v.real, v.imag)
    glEnd()
def main():
    t = 0
    dT = 2
    smen = 100
    pygame.init()
    frRate = 0.001
    count = 1
    display = (800,800)
    gluOrtho2D(0, 30 * display[0] / display[1], 0, 30)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        VisCenter(0)
        VisFunc(lambda x : x + 0.01, Inv)
        VisFunc(lambda x : x + 0.01, lambda x : x / m)
        pygame.display.flip()
        pygame.time.wait(dT)

        
main() 