from __future__ import annotations
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from math import *
from glfw import *
import time

init()
winWidth = 1000
winHeight = 1000
window = create_window(winWidth, winHeight, "???", None, None)
make_context_current(window)
glViewport(0, 0, winWidth, winHeight)
def Vis(m: list[complex]):
    glBegin(GL_LINE_STRIP)
    for i in m:
        glVertex2d(i.real, i.imag)
    glEnd()
def RotateSphere(m, infinity, zero):
    ansM = []
    for i in m:
        if i - infinity == 0:
            ansM.append(inf)
        else:
            ansM.append(complex(zero - i) / complex(infinity - i))
    return ansM
def Spiral(density, n, px, py):
    pos = px + 1j * py
    i, j = pos, pos
    m = []
    angle = pi / 20 
    k = 1.04 * e ** (1j * angle/ density)
    while len(m) < 2 * n:
        m = [i] + m + [j]
        i *= k
        j *= 1 / k
    return m
x: GLint = 0
y: GLint = 0
frameRate = 1
speed = 10
def zoomScale():
    flag = False
    global x
    global y
    if get_key(window, KEY_D):
        x -= speed * frameRate
        flag = True
    elif get_key(window, KEY_A):
        x += speed * frameRate
        flag = True
    if get_key(window, KEY_W):
        y -= speed * frameRate
        flag = True
    elif get_key(window, KEY_S):
        y += speed * frameRate
        flag = True
    if flag:
        glViewport(x, y, winWidth, winHeight)
def DrawAxes():
    glBegin(GL_LINES)
    glVertex2d(1, 0)
    glVertex2d(-1, 0)
    glEnd()
    glPointSize(5)
    glColor4dv(1, 0, 0, 1)
    glBegin(GL_POINTS)
    glVertex2d(1, 0)
    glEnd()
    glColor4dv(0, 1, 0, 1)
    glBegin(GL_POINTS)
    glVertex2d(-1, 0)
    glEnd()
def Trans(m, k):
    ansM = []
    for i in m:
        ansM.append(i + k)
    return ansM
def zoomzoom(m, k):
    ansM = []
    for i in m:
        ansM.append(i * k)
    return ansM
k = 0.01
r = 0
m = zoomzoom(RotateSphere(Spiral(1, 6000, -1, 0), 1, -1), 0.3)
while window_should_close(window) == FALSE:
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(0, 0, 1, 0.5)
    zoomScale()
    #m = Trans(m, k)
    #r += k
    #if r > 1 or r < -1:
    #    k = -k
    Vis(m)
    swap_buffers(window)
    poll_events()
    #time.sleep(1 / frameRate)