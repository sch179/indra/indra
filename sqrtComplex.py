import cmath
import math

def SolveSqrtMax(a, b, c):
    D = b ** 2 - a * c * 4
    if D >= 0:
        return (-b + math.sqrt(D)) / (2 * a)
def CSqrt(c: complex) -> complex:
    x = c.real
    y = c.imag
    v = math.sqrt(SolveSqrtMax(4, 4 * x, -(y ** 2)))
    u = y / (2 * v)
    return u + 1j * v
print(CSqrt(2 + 1j))
print(cmath.sqrt(2 + 1j))