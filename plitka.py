from __future__ import annotations
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from numpy import *
from typing import *
import cmath
x = 0.1
def VisTriangles(m: List[complex]) -> None:
    glBegin(GL_LINE_STRIP)
    for i in m:
        glVertex2d(i.real, i.imag)
def RotateTriangles(m: List[complex], n: int = 0) -> List[complex]:
    mRet = []
    for i in m:
        mRet.append(i * e ** (1j * pi / 3))
    return mRet
def MoveTriangles(m, k=1):
    mRet = []
    for i in m:
        mRet.append(i + k * (x * sqrt(3) * e ** (pi/3 * 1j)))
    return mRet
def main():
    t = 0
    treugs = [[0, 1j * x, 1j * x]]
    dT = 2
    smen = 100
    pygame.init()
    frRate = 0.001
    count = 1
    display = (800,800)
    gluOrtho2D(0, 30 * display[0] / display[1], 0, 30)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        
        pygame.display.flip()
        pygame.time.wait(dT)

        
main() 