from __future__ import annotations
from typing import Any
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from numpy import *
from glfw import *
import cmath

#elems: list[list[complex]] = []
grid: list[list[complex]] = []
globScale = 1
def T(e: Any, c: complex):
    match e:
        case complex():
            return e + c
        case list():
            match e[0]:
                case complex():
                    el = []
                    for i in e:
                        el.append(i + c)
                    return el
                case list():
                    elems = []
                    for i in e:
                        elelel = []
                        for elel in i:
                            elelel.append(elel + c)
                        elems.append(elelel)
                case None:
                    return []
                


def MakeGrid(scale: float = 1, center: complex = - 1 - 1 * 1j, n: int = 40) -> list[list[complex]]:
    c1 = center
    c2 = center
    elems = []
    step = 0.05
    rang = n
    e = []
    for i in range(n):
        curLine1 = [c1, c1 + rang]
        curLine2 = [c2, c2 + rang * 1j]
        elems.append(curLine1)
        elems.append(curLine2)
        c1 += step * 1j
        c2 += step
    return elems
    
def Euler(T: matrix, c: complex) -> complex:
    if T[1][0] * c + T[1][1] != 0:
        return (T[0][0] * c + T[0][1]) / (T[1][0] * c + T[1][1])
    else:
        return complex(inf)

def EulerFunc(elems: list[list[complex]], T: matrix) -> list[list[complex]]:
    ret = []
    for i in elems:
        ad = []
        for e in i:
            ad.append(Euler(T, e))
        ret.append(ad)
    return ret
def GenMatrixEu(c1: complex, c2: complex) -> matrix:
    a: complex = 1
    b = - a * c1
    c: complex = 1
    d = - c * c2
    return [[a, b], [c, d]]

def Move(vec: complex, elems: list[list[complex]]):
    ret = []
    for i in elems:
        ad = []
        for e in i:
            ad.append(e + vec)
        ret.append(ad)
    return ret
def Vis(elems: list[list[complex]]):
    for a in elems:
        glBegin(GL_LINE_STRIP)
        for i in a:
            glVertex2d(i.real, i.imag)
        glEnd()




def Line(step: complex, center: complex, le: float) -> list[complex]:
    c = center
    ret = []
    for i in range(int(le//(sqrt(step.real ** 2 + step.imag ** 2)))):
        ret.append(c)
        c += step
    return ret
def CutTrail(trail: list[list[complex]]) -> list[list[complex]]:
    if len(trail) > 20:
        return trail[:-1]
    else:
        return trail
    return trail
init()
winWidth = 1000
winHeight = 1000
window = create_window(winWidth, winHeight, "???", None, None)
make_context_current(window)
glViewport(0, 0, winWidth, winHeight)

m = [Line(0.01, 0.1, 100)]
v = 0.1
def Inversion(elems: list[list[complex]]) -> list[list[complex]]:
    ret = []
    for i in elems:
        ad = []
        for e in i:
            if e != 0:
                ad.append(1 / e)
            else:
                ad.append(inf)
        ret.append(ad)
    return ret
dvig = -5.1 - 5.1j
iter = 0
while window_should_close(window) == FALSE:
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(0, 0, 1, 0.5)
    #Vis(MakeGrid)
    iter += 1
    if iter % 10 == 0:
        dvig = dvig * -1
    m = Inversion(Move(dvig, Inversion(m)))
    Vis(m)
    m.append(Line(0.01, v, 100))
    v *= v
    m = CutTrail(m)
    swap_buffers(window)
    poll_events()