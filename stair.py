from __future__ import annotations
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
import cmath
def gVisPolyComp(m: list):
    def gCompVert(c: complex):
        glVertex2d(c.real, c.imag)
    glBegin(GL_LINE_STRIP)
    for i in m:
        gCompVert(i)
    if len(m) > 2:
        gCompVert(m[0])
    glEnd()

def gNPolygon(n, c, r):
    polygon = [r + 0j + c]
    k =  pi * (n - 2) * n
    for i in range(0, n - 1):
        v = polygon[i] * (cos(k) + sin(k) * 1j)
        polygon.append()
    return polygon


def main():
    t = 0
    dT = 2
    smen = 100
    hex = [[1 + 0j, 0 + 1j, 0.5 + -0.5j]]
    pygame.init()
    frRate = 0.001
    count = 1
    display = (800,600)
    gluOrtho2D(0, 30 * display[0] / display[1], 0, 30)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        
        if t % smen == 0:
            hex.append(hex[count - 1])
            hex[count] = [i * 0.5 * (cos(pi / 4) + sin(pi / 4) * 1j) for i in hex[count]]
            if count < 8:
                count += 1
            else:
                hex.pop(0)
        if t > smen * 4:
            for i in range(len(hex)):
                for j in range(len(hex[i])):
                    hex[i][j] = hex[i][j] * (2 ** (1 / smen))
        for i in hex:
            gVisPolyComp(i)
        t += 1
        pygame.display.flip()
        pygame.time.wait(dT)
        
main() 
