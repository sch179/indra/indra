from __future__ import annotations
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from glfw import *
from numpy import *

pos = 1
start = 10.11 + 0.01j
end = 0
step = (sin(pi / 50000) + 1j * cos(pi / 50000)) * 1.0000001
init()
zoom = 100
def Arg(c: complex) -> float:
    return float(sqrt(c.real ** 2 + c.imag ** 2))
def Det(m: matrix) -> complex:
    return m.A1[0] * m.A1[3] - m.A1[1] * m.A1[2]
def Rotate(x: complex) -> complex:
    return x * e ** (1j * 0.1) * 1.001
def MnojMatrix(c: complex) -> matrix:
    return [[sqrt(c), 0], [0, 1 / sqrt(c)]]
def SoprMatrix(start: complex, end: complex):
    m = matrix([[1, -start], [1, -end]])
    m = m * sqrt(Det(m))
    return m
def LoxMatrix(start: complex, end: complex, step: matrix):
    return SoprMatrix(start, end) * MnojMatrix(step) * SoprMatrix(start, end)
def RevMatrix(m: matrix) -> matrix:
    return matrix([m[1][1], -m[0][1]][-m[1][0], m[0][0]])
def Meb(m: matrix, c: complex) -> complex:
    return ((c * complex(m.A1[0]) + complex(m.A1[1])) / (c * complex(m.A1[2]) + complex(m.A1[3])))
def MoveSopr(c: complex, start: complex, end: complex):
    global step
    if Arg(trail[0] - trail[1]) < zoom * 0.001:
        step = 1 / step
    rc = Meb(LoxMatrix(start, end, step), c)
    return rc
def GetImportantDots():
    glLineWidth(10)
    glBegin(GL_LINE_STRIP)
    glVertex2d(0.5, 0.5)
    glVertex2d(1, 0)
    glVertex2d(0, 0)
    glEnd()
    return
def VisTrail(trail: list(complex)) -> None:
    glPointSize(5)
    glBegin(GL_POINTS)
    for i in trail:
        c = i
        c = c / zoom
        glVertex2d(c.real, c.imag)
    glEnd()
def CutTrail(trail: list(complex)) -> list(complex):
    if len(trail) > 30:
        return trail[-30:]
    else:
        return trail
    return trail
def f(x: complex) -> complex:
    return x
def AlteredTrail(f, trail: list(complex)):
    glPointSize(5)
    glBegin(GL_POINTS)
    for i in trail:
        c = f(i)
        c = c / zoom
        glVertex2d(c.real, c.imag)
    print(c)
    glEnd()
def MoveTrail(trail: list(complex)) -> list(complex):
    trail.append(MoveSopr(trail[-1], start, pos))
    trail = CutTrail(trail)
    glColor(1, 0, 0, 1)
    VisTrail(trail)
    glColor(1, 1, 1, 1)
    return trail
window = create_window(600, 600, "???", None, None)
make_context_current(window)
trail: list(complex) = list([0.5 + 0.5 * 1j, 0.5 + 0.5 * 1j])
while window_should_close(window) == FALSE:
    pos =  get_cursor_pos(window)[0] / 600 - get_cursor_pos(window)[1] * 1j / 600
    glPointSize(100)
    glBegin(GL_POINTS)
    glVertex2d(pos.real, pos.imag)
    glEnd()
    GetImportantDots()
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(0, 0, 1, 0.5)
    trail = MoveTrail(trail)
    AlteredTrail(f, trail)
    swap_buffers(window)
    poll_events()